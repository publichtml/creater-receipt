# creater-receipt  
小票生成并打印  
1. 在报销系统中查看审批状态，选择你要打印的报销单，点击打印，在新弹出的页面中，右键选择保存页面（如xxx.htm）  
2. 在小票目录下执行命令python create.py xxx.htm即完成小票的生成和打印  
---  
3. 不需要打印，只需要生成文档时，python create_without_print.py xxx.htm即可生成小票文档  

---

# 不通过html直接生成文档  
- create_without_print.vbs 文档模板 金额 日期  
- create_without_print.vbs wgk.doc 557 2018-10-28 1000 2018-11-04 1000 2018-11-09 1000 2018-11-14  

---

# 依赖安装  
- python3安装  
- 安装pip3: python get-pip.py  
- 安装pyquery: pip install pyquery  

